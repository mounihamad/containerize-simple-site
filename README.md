# Containerize a simple site with docker

## You most have

Docker installed in your computer

## Build the imagme

After clonning this repo, go to the folder, in the commande line, type the next commade
for building an images who content the site code

```
docker build -t imageName:v1 .
```

Check if the images there whith the next commande:

```
docker images
```

## Creating and runing the container

Create the container:

```
docker run --name containerName -d -p 80:80 imageName:v1
```

Check that the container is runing whith the next commande

```
docker ps
```

## Open the app

If u work in local machine you can type localhost in your brower.
If you are in the remote server, you can type IPAdress or yourDomainName in the brower.
